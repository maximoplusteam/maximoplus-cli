#!/usr/bin/env node

const ip = require("ip");
const simpleGit = require("simple-git/promise")();
const replace = require("replace-in-file");
const { red, white, blue, bold, green, magenta } = require("kleur");
const ora = require("ora");
const inquirer = require("inquirer");
const boxen = require("boxen");

let ipAddress = ip.address();
const projectURL =
  "https://bitbucket.org/maximoplusteam/rntemplate.git";
const initProject = async () => {
  let spinner;
  let port;
  let projectName;
  try {
    projectName = "rnproject"; //should come from the form
    console.log("Your IP addrress is " + bold().magenta(ipAddress));
    console.log(
      "Ensure your MaximoPlus Server is up and running and enter the required application properties"
    );
    const inqProms = await inquirer.prompt([
      {
        type: "input",
        name: "name",
        message: "New MaximoPlus Project Name",
        default: "maximoplus_project"
      },
      {
        type: "input",
        name: "ip",
        message: "Enter the MaximoPlus Server IP Address or URL",
        default: ipAddress
      },
      {
        type: "input",
        name: "port",
        message: "Enter the MaximoPlus Server Port",
        default: "8080"
      }
    ]);
    projectName = inqProms.name.replace(/\s/g, "_");
    port = parseInt(inqProms.port);
    ipAddress = inqProms.ip;
    console.log(bold().red("Downloading the project files, please wait"));
    spinner = ora("Loading...").start();
    await simpleGit.clone(projectURL, projectName);
    await replace({
      files: projectName + "/.env",
      from: "192.168.0.146",
      to: ipAddress
    });
    await replace({
      files: projectName + "/.env",
      from: "maximoplus-demo",
      to: projectName
    });
    await replace({
      files: projectName + "/.env",
      from: "9009",
      to: port
    });
  } catch (e) {
    console.log(e);
  }
  spinner.stop();
  const message = `
The installation finished successfully. Next steps:
- ${bold().magenta("cd " + projectName)}
- Install dependencies with ${bold().magenta("yarn install")}
- Start the Expo Server with ${bold().magenta("yarn start")}
- Open the Expo app on the device or emulator, and connect it to the Expo server.
`;
  console.log(boxen(message, { padding: 1, margin: 1 }));
};

initProject();
